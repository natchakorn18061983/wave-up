New in 3.1.2
★ Update some translations. Thanks again for all the volunteer
  translations!

New in 3.1.1
★ Fix bug while trying to report an issue if several email apps where
  installed.

New in 3.1.0
★ Prepare for WaveUp Tasker Plugin (coming soon)!
★ Update some translations.

New in 3.0.15
★ Try to start service faster to avoid Android > Oreo forcing us to stop.
  This will hopefully avoid some seldom crashes.
★ Update Russian translation.
