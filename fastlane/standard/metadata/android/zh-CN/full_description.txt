挥手亮屏是一款当你<i>挥过</i>距离传感器时<i>唤醒你的手机</i> - 点亮屏幕的应用。

我开发这款应用是想避免只为看时间而按电源键，而这种事我经常干。类似的应用已经有很多了，甚至功能更为强大。我曾被一款<b>很棒</b>的应用 - 重力锁屏 - 开/关 - 打动。然而我是一个开源软件的狂热粉丝，并且会尽一切可能在手机里安装自由软件。在此之前没有一款这类的开源应用，因此我开发了挥手亮屏。如果你感兴趣可以在这里看源代码：
https://gitlab.com/juanitobananas/wave-up

只要挥过手机的距离传感器，你就能点亮屏幕。这被称为<i>挥手模式</i>并且可以在设置中禁用以避免意外亮屏。

当你从口袋或钱包中拿出手机时同样会点亮屏幕。这被称为<i>口袋模式</i>并且也可以在设置中禁用。

这两个模式默认启用。

如果你遮挡距离传感器1秒钟（或设定的时间），它也会锁屏。该功能没有具体名称但同样可以在设置中调整。该功能默认不启用。

如果你没听说过距离传感器：它是一个小部件，位于听筒附近的某个位置。你实际看不见它，它负责在你打电话时告诉手机关闭屏幕。

<b>卸载</b>

本应用使用了设备管理员权限。因此你无法“正常”卸载挥手亮屏。

要卸载它，只需打开应用并在菜单底部点击“卸载挥手亮屏”按钮即可。

<b>已知问题</b>

不幸的是，一些手机会在监听距离传感器时运行CPU。这被称为<i>唤醒锁</i>并且会造成大量耗电。这不是我的问题，我对此无能为力。其他手机会在屏幕关闭时“休眠”但仍然监听距离传感器。这种情况下，电量消耗几乎为0。

<b>需要的安卓权限</b>

▸ WAKE_LOCK 用来点亮屏幕
▸ USES_POLICY_FORCE_LOCK 用来锁定设备
▸ RECEIVE_BOOT_COMPLETED 用来开机自启动（如果被勾选）
▸ READ_PHONE_STATE 用来在打电话时暂时禁用挥手亮屏

<b>杂记</b>

这是我写的第一个安卓应用，因此请注意！

这也是我给开源世界的第一个微小的贡献。终于！

如果你们能提供给我任何建议或者任何形式的帮助，我将感激不尽！

感谢阅读！

开源万岁！！！

<b>翻译</b>

如果你能帮忙将WaveUp翻译成你的语言，那就太棒了(甚至英文版也可能会被修改)。
在Transifex上有两个项目可以翻译: https://www.transifex.com/juanitobananas/waveup/ 和https://www.transifex.com/juanitobananas/libcommon/.

<b>致谢</b>

特别感谢：

参见：https://gitlab.com/juanitobananas/wave-up/#acknowledgments